Source: totem
Section: video
Priority: optional
Maintainer: Ubuntu Desktop Team <ubuntu-desktop@lists.ubuntu.com>
XSBC-Original-Maintainer: Sebastien Bacher <seb128@debian.org>
Uploaders: @GNOME_TEAM@
Build-Depends: debhelper (>= 5.0.51~),
               cdbs (>= 0.4.90),
               python (>= 2.6.6-3~),
               python-gi-dev (>= 2.90.3),
               libglib2.0-dev (>= 2.27.92),
               libatk1.0-dev (>= 1.29.4),
               libgdk-pixbuf2.0-dev (>= 2.23.0),
               libgtk-3-dev (>= 3.0.0),
               libtotem-plparser-dev (>= 2.32.4-2),
               libsoup2.4-dev,
               libpeas-dev (>= 0.7.2),
               libxml2-dev (>= 2.6.0),
               liblircclient-dev (>= 0.6.6),
               libirman-dev (>= 0.4.2),
               gnome-pkg-tools (>= 0.10),
               gtk-doc-tools,
               scrollkeeper,
               libgstreamer0.10-dev (>= 0.10.30),
               libgstreamer-plugins-base0.10-dev (>= 0.10.30),
               gstreamer0.10-tools (>= 0.10.26),
               gstreamer0.10-plugins-base (>= 0.10.26),
               gstreamer0.10-plugins-good,
               gstreamer0.10-gconf,
               librsvg2-dev (>= 2.16.0-2),
               librsvg2-common (>= 2.16.0-2),
               libnautilus-extension-dev (>= 2.91.4),
               gnome-icon-theme (>= 2.15.90),
               libdbus-glib-1-dev (>= 0.82),
               intltool (>= 0.40),
               autotools-dev,
               dpkg-dev (>= 1.13.19),
               shared-mime-info (>= 0.22),
               libcam-dev [kfreebsd-any],
               libxrandr-dev (>= 1.1.1),
               libxxf86vm-dev (>= 1.0.1),
               libx11-dev,
               x11proto-core-dev,
               gnome-doc-utils (>= 0.20.3),
               libbluetooth-dev [linux-any],
               libgdata-dev (>= 0.8.0),
               gobject-introspection (>= 0.9.12-4~),
               libgirepository1.0-dev (>= 0.9.12),
               liblaunchpad-integration-3.0-dev,
               hardening-wrapper,
               dh-autoreconf,
               gnome-common,
               valac,
               libzeitgeist-dev,
Standards-Version: 3.8.4
Homepage: http://www.gnome.org/projects/totem/
Vcs-Git: git://gitorious.org/ubuntu-omap/totem.git

Package: libtotem0
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Replaces: totem (<< 3.0.1-1)
Description: Main library for the Totem media player
 This package contains the main library used by the Totem media player. It is
 used both by the media player itself and by the plugins.
 .
 This library is only useful within Totem. You should not have to
 manually install this package.


Package: totem
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         ${python:Depends},
         gstreamer0.10-plugins-base (>= 0.10.26),
         gstreamer0.10-alsa | gstreamer0.10-audiosink,
         gstreamer0.10-plugins-good (>= 0.10.7),
         gstreamer0.10-x,
         gnome-icon-theme (>= 2.15.90),
         totem-common (= ${source:Version})
Conflicts: totem (<< 0.99.12-2),
           gnome-control-center (<< 2.15.90),
           totem-mozilla (<< 2.20.0-3),
           totem-gstreamer (<< 2.27.1)
Recommends: totem-plugins,
            gnome-icon-theme-symbolic,
            totem-mozilla
Suggests: gnome-codec-install,
          gstreamer0.10-plugins-ugly,
          gstreamer0.10-plugins-bad,
          gstreamer0.10-ffmpeg,
          gstreamer0.10-pulseaudio (>= 0.10.16-5)
Description: Simple media player for the GNOME desktop based on GStreamer
 Totem is a simple yet featureful media player for GNOME which can read
 a large number of file formats. It features :
 .
    * Shoutcast, m3u, asx, SMIL and ra playlists support
    * DVD (with menus), VCD and Digital CD (with CDDB) playback
    * TV-Out configuration with optional resolution switching
    * 4.0, 5.0, 5.1 and stereo audio output
    * Full-screen mode (move your mouse and you get nice controls) with
      Xinerama, dual-head and RandR support
    * Aspect ratio toggling, scaling based on the video's original size
    * Full keyboard control
    * Simple playlist with repeat mode and saving feature
    * GNOME, Nautilus and GIO integration
    * Screenshot of the current movie
    * Brightness and Contrast control
    * Visualisation plugin when playing audio-only files
    * Video thumbnailer for nautilus
    * Nautilus properties page
    * Works on remote displays
    * DVD, VCD and OGG/OGM subtitles with automatic language selection
    * Extensible with plugins

Package: totem-mozilla
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends},
         totem (= ${binary:Version}),
         dbus-x11 (>= 0.61)
Recommends: firefox | epiphany-browser | www-browser
XB-Npp-Applications: ec8030f7-c20a-464f-9b0e-13a3a9e97384, 92650c4d-4b8e-4d2a-b7eb-24ecf4f6b63a
XB-Npp-Name:  Totem Mozilla plugin
XB-Npp-MimeType: video/quicktime, video/mp4, image/x-macpaint, image/x-quicktime, application/x-mplayer2, video/x-ms-asf-plugin, video/x-msvideo, video/x-ms-asf, video/x-ms-wmv, video/x-wmv, video/x-ms-wvx, video/x-ms-wm, application/ogg, video/mpeg, audio/wav, audio/x-wav, audio/mpeg
XB-Npp-Description: Watch Movies in your Browser (http://projects.gnome.org/totem/)
XB-Npp-Filename: libtotem-
Description: Totem Mozilla plugin
 This package contains the Totem Mozilla plugin, which will
 enhance your Gecko-based browser to be able to display movie
 clips.
 .
 This plugin should work for Firefox as well as XULRunner based browsers.

Package: totem-common
Architecture: all
Depends: ${misc:Depends}
Description: Data files for the Totem media player
 Totem is a simple yet featureful media player for GNOME which can read
 a large number of file formats.
 .
 This package contains common data files and translations.

Package: totem-dbg
Architecture: any
Section: debug
Priority: extra
Depends: totem (= ${binary:Version}),
         ${misc:Depends}
Conflicts: libtotem-plparser1-dbg, libtotem-plparser7-dbg
Description: Debugging symbols for the Totem media player
 Totem is a simple yet featureful media player for GNOME which can read
 a large number of file formats.
 .
 This package contains detached debugging symbols.

Package: totem-plugins
Architecture: any
Depends: totem (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
         ${python:Depends},
         gir1.2-totem-1.0 (= ${binary:Version}),
         gir1.2-gtk-3.0,
         gir1.2-gdkpixbuf-2.0,
         gir1.2-glib-2.0,
         gir1.2-pango-1.0,
         gir1.2-peas-1.0,
         python-gi,
         python-xdg,
         python-httplib2
Recommends: gnome-settings-daemon
Suggests: gromit
Description: Plugins for the Totem media player
 Totem is a simple yet featureful media player for GNOME which can read
 a large number of file formats.
 .
 This package contains a set of recommended plugins for Totem, which
 allow to:
 .
    * Control Totem with an Infrared remote control
    * Control Totem with the keyboard's media player keys
    * Keep the Totem window on top of the screen
    * Display movie properties
    * Deactivate the screensaver when a movie is playing
    * Skip to a defined time in the movie
    * Set the away status in the instant messenger when a movie is
      playing
    * Control totem with a mobile phone using the Bluetooth protocol
    * Share the current playlist via HTTP
    * Search, browse for and play videos from YouTube
 .
 Additional plugins can be written in C, Python or Vala.

Package: totem-plugins-extra
Architecture: any
Depends: totem (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
         python-beautifulsoup,
         python-feedparser
Recommends: gromit
Replaces: totem-xine (<< 2.20.0-3),
          totem-gstreamer (<< 2.20.0-3),
          totem-plugins (<< 2.27.92-0ubuntu2),
          totem-plugins-extra (<< 2.27.1-0ubuntu1)
Description: Extra plugins for the Totem media player
 Totem is a simple yet featureful media player for GNOME which can read
 a large number of file formats.
 .
 This package contains an extra set plugins for Totem, which allow to:
 .
    * Annotate the screen with the Gromit tool
    * Stream BBC programs
 .
 Additional plugins can be written in C, Python or Vala.

Package: gir1.2-totem-1.0
Section: libs
Architecture: any
Depends: ${gir:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Description: GObject introspection data for Totem media player
 Totem is a simple yet featureful media player for GNOME which can read
 a large number of file formats.
 .
 This package contains introspection data for the Totem media player.
 .
 It can be used by packages using the GIRepository format to generate
 dynamic bindings.

Package: libtotem-dev
Section: libdevel
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         libtotem0 (= ${binary:Version}),
         gir1.2-totem-1.0 (= ${binary:Version}),
         libglib2.0-dev,
         libgtk-3-dev,
         libtotem-plparser-dev
Replaces: totem (<< 3.0.1-0ubuntu5)
Description: Main library for the Totem media player - development files
 This package contains development files for the Totem media player library.
 .
 You may need it to develop plugins for Totem.
