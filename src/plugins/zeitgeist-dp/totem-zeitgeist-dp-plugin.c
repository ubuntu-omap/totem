/* totem-zeitgeist-dp-plugin.c generated by valac 0.11.7, the Vala compiler
 * generated from totem-zeitgeist-dp-plugin.vala, do not modify */


#include <glib.h>
#include <glib-object.h>
#include <stdlib.h>
#include <string.h>
#include <totem.h>
#include <libpeas/peas.h>
#include <zeitgeist.h>
#include <gio/gio.h>
#include <bacon-video-widget.h>


#define TYPE_MEDIA_INFO (media_info_get_type ())
typedef struct _MediaInfo MediaInfo;
#define _g_free0(var) (var = (g_free (var), NULL))

#define TYPE_ZEITGEIST_DP_PLUGIN (zeitgeist_dp_plugin_get_type ())
#define ZEITGEIST_DP_PLUGIN(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_ZEITGEIST_DP_PLUGIN, ZeitgeistDpPlugin))
#define ZEITGEIST_DP_PLUGIN_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_ZEITGEIST_DP_PLUGIN, ZeitgeistDpPluginClass))
#define IS_ZEITGEIST_DP_PLUGIN(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_ZEITGEIST_DP_PLUGIN))
#define IS_ZEITGEIST_DP_PLUGIN_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_ZEITGEIST_DP_PLUGIN))
#define ZEITGEIST_DP_PLUGIN_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_ZEITGEIST_DP_PLUGIN, ZeitgeistDpPluginClass))

typedef struct _ZeitgeistDpPlugin ZeitgeistDpPlugin;
typedef struct _ZeitgeistDpPluginClass ZeitgeistDpPluginClass;
typedef struct _ZeitgeistDpPluginPrivate ZeitgeistDpPluginPrivate;
#define _g_object_unref0(var) ((var == NULL) ? NULL : (var = (g_object_unref (var), NULL)))
#define _g_ptr_array_free0(var) ((var == NULL) ? NULL : (var = (g_ptr_array_free (var, TRUE), NULL)))
#define _g_error_free0(var) ((var == NULL) ? NULL : (var = (g_error_free (var), NULL)))
typedef struct _ZeitgeistDpPluginQueryMediaMimetypeData ZeitgeistDpPluginQueryMediaMimetypeData;

struct _MediaInfo {
	gint64 timestamp;
	gboolean sent_access;
	gchar* mrl;
	gchar* mimetype;
	gchar* title;
	gchar* interpretation;
	gchar* artist;
	gchar* album;
};

struct _ZeitgeistDpPlugin {
	GObject parent_instance;
	ZeitgeistDpPluginPrivate * priv;
};

struct _ZeitgeistDpPluginClass {
	GObjectClass parent_class;
};

struct _ZeitgeistDpPluginPrivate {
	MediaInfo current_media;
	guint media_info_timeout;
	guint timeout_id;
	gulong* signals;
	gint signals_length1;
	gint _signals_size_;
	ZeitgeistLog* zg_log;
	ZeitgeistDataSourceRegistry* zg_registry;
	TotemObject* _object;
};

struct _ZeitgeistDpPluginQueryMediaMimetypeData {
	int _state_;
	GObject* _source_object_;
	GAsyncResult* _res_;
	GSimpleAsyncResult* _async_result;
	ZeitgeistDpPlugin* self;
	gchar* current_mrl;
	gchar* _tmp0_;
	gchar* mrl;
	GFile* _tmp1_;
	GFile* f;
	GFileInfo* _tmp2_;
	GFileInfo* fi;
	gboolean _tmp3_;
	gboolean _tmp4_;
	const gchar* _tmp5_;
	gchar* _tmp6_;
	GError * err;
	GError * _inner_error_;
};


static gpointer zeitgeist_dp_plugin_parent_class = NULL;
static PeasActivatableInterface* zeitgeist_dp_plugin_peas_activatable_parent_iface = NULL;
static GType zeitgeist_dp_plugin_type_id = 0;

GType media_info_get_type (void) G_GNUC_CONST;
MediaInfo* media_info_dup (const MediaInfo* self);
void media_info_free (MediaInfo* self);
void media_info_copy (const MediaInfo* self, MediaInfo* dest);
void media_info_destroy (MediaInfo* self);
GType zeitgeist_dp_plugin_get_type (void) G_GNUC_CONST;
GType zeitgeist_dp_plugin_register_type (GTypeModule * module);
#define ZEITGEIST_DP_PLUGIN_GET_PRIVATE(o) (G_TYPE_INSTANCE_GET_PRIVATE ((o), TYPE_ZEITGEIST_DP_PLUGIN, ZeitgeistDpPluginPrivate))
enum  {
	ZEITGEIST_DP_PLUGIN_DUMMY_PROPERTY,
	ZEITGEIST_DP_PLUGIN_OBJECT
};
static void zeitgeist_dp_plugin_real_activate (PeasActivatable* base);
TotemObject* zeitgeist_dp_plugin_get_object (ZeitgeistDpPlugin* self);
static void zeitgeist_dp_plugin_file_opened (ZeitgeistDpPlugin* self, const gchar* mrl, TotemObject* totem);
static void _vala_array_add1 (gulong** array, int* length, int* size, gulong value);
static void zeitgeist_dp_plugin_file_closed (ZeitgeistDpPlugin* self, TotemObject* totem);
static void _vala_array_add2 (gulong** array, int* length, int* size, gulong value);
static void zeitgeist_dp_plugin_metadata_changed (ZeitgeistDpPlugin* self, const gchar* artist, const gchar* title, const gchar* album, guint track_num, TotemObject* totem);
static void _vala_array_add3 (gulong** array, int* length, int* size, gulong value);
static void zeitgeist_dp_plugin_playing_changed (ZeitgeistDpPlugin* self);
static void _vala_array_add4 (gulong** array, int* length, int* size, gulong value);
static void zeitgeist_dp_plugin_real_deactivate (PeasActivatable* base);
static void zeitgeist_dp_plugin_real_update_state (PeasActivatable* base);
static void zeitgeist_dp_plugin_restart_watcher (ZeitgeistDpPlugin* self, guint interval);
static gboolean zeitgeist_dp_plugin_timeout_cb (ZeitgeistDpPlugin* self);
static gboolean _zeitgeist_dp_plugin_timeout_cb_gsource_func (gpointer self);
static gboolean zeitgeist_dp_plugin_wait_for_media_info (ZeitgeistDpPlugin* self);
static gboolean _zeitgeist_dp_plugin_wait_for_media_info_gsource_func (gpointer self);
static void zeitgeist_dp_plugin_send_event_to_zg (ZeitgeistDpPlugin* self, gboolean leave_event);
static void zeitgeist_dp_plugin_query_media_mimetype_data_free (gpointer _data);
static void zeitgeist_dp_plugin_query_media_mimetype (ZeitgeistDpPlugin* self, const gchar* current_mrl, GAsyncReadyCallback _callback_, gpointer _user_data_);
static void zeitgeist_dp_plugin_query_media_mimetype_finish (ZeitgeistDpPlugin* self, GAsyncResult* _res_);
static gboolean zeitgeist_dp_plugin_query_media_mimetype_co (ZeitgeistDpPluginQueryMediaMimetypeData* data);
static void zeitgeist_dp_plugin_query_media_mimetype_ready (GObject* source_object, GAsyncResult* _res_, gpointer _user_data_);
ZeitgeistDpPlugin* zeitgeist_dp_plugin_new (void);
ZeitgeistDpPlugin* zeitgeist_dp_plugin_construct (GType object_type);
void zeitgeist_dp_plugin_set_object (ZeitgeistDpPlugin* self, TotemObject* value);
static void zeitgeist_dp_plugin_finalize (GObject* obj);
static void _vala_zeitgeist_dp_plugin_get_property (GObject * object, guint property_id, GValue * value, GParamSpec * pspec);
static void _vala_zeitgeist_dp_plugin_set_property (GObject * object, guint property_id, const GValue * value, GParamSpec * pspec);
void peas_register_types (GTypeModule* module);


void media_info_copy (const MediaInfo* self, MediaInfo* dest) {
	dest->timestamp = self->timestamp;
	dest->sent_access = self->sent_access;
	dest->mrl = g_strdup (self->mrl);
	dest->mimetype = g_strdup (self->mimetype);
	dest->title = g_strdup (self->title);
	dest->interpretation = g_strdup (self->interpretation);
	dest->artist = g_strdup (self->artist);
	dest->album = g_strdup (self->album);
}


void media_info_destroy (MediaInfo* self) {
	_g_free0 ((*self).mrl);
	_g_free0 ((*self).mimetype);
	_g_free0 ((*self).title);
	_g_free0 ((*self).interpretation);
	_g_free0 ((*self).artist);
	_g_free0 ((*self).album);
}


MediaInfo* media_info_dup (const MediaInfo* self) {
	MediaInfo* dup;
	dup = g_new0 (MediaInfo, 1);
	media_info_copy (self, dup);
	return dup;
}


void media_info_free (MediaInfo* self) {
	media_info_destroy (self);
	g_free (self);
}


GType media_info_get_type (void) {
	static volatile gsize media_info_type_id__volatile = 0;
	if (g_once_init_enter (&media_info_type_id__volatile)) {
		GType media_info_type_id;
		media_info_type_id = g_boxed_type_register_static ("MediaInfo", (GBoxedCopyFunc) media_info_dup, (GBoxedFreeFunc) media_info_free);
		g_once_init_leave (&media_info_type_id__volatile, media_info_type_id);
	}
	return media_info_type_id__volatile;
}


static void _vala_array_add1 (gulong** array, int* length, int* size, gulong value) {
	if ((*length) == (*size)) {
		*size = (*size) ? (2 * (*size)) : 4;
		*array = g_renew (gulong, *array, *size);
	}
	(*array)[(*length)++] = value;
}


static void _vala_array_add2 (gulong** array, int* length, int* size, gulong value) {
	if ((*length) == (*size)) {
		*size = (*size) ? (2 * (*size)) : 4;
		*array = g_renew (gulong, *array, *size);
	}
	(*array)[(*length)++] = value;
}


static void _vala_array_add3 (gulong** array, int* length, int* size, gulong value) {
	if ((*length) == (*size)) {
		*size = (*size) ? (2 * (*size)) : 4;
		*array = g_renew (gulong, *array, *size);
	}
	(*array)[(*length)++] = value;
}


static void _vala_array_add4 (gulong** array, int* length, int* size, gulong value) {
	if ((*length) == (*size)) {
		*size = (*size) ? (2 * (*size)) : 4;
		*array = g_renew (gulong, *array, *size);
	}
	(*array)[(*length)++] = value;
}


static gpointer _g_object_ref0 (gpointer self) {
	return self ? g_object_ref (self) : NULL;
}


static void zeitgeist_dp_plugin_real_activate (PeasActivatable* base) {
	ZeitgeistDpPlugin * self;
	ZeitgeistLog* _tmp0_ = NULL;
	ZeitgeistDataSourceRegistry* _tmp1_ = NULL;
	MediaInfo _tmp2_ = {0};
	MediaInfo _tmp3_ = {0};
	gulong _tmp4_;
	gulong _tmp5_;
	gulong _tmp6_;
	gulong _tmp7_;
	GPtrArray* _tmp8_ = NULL;
	GPtrArray* templates;
	ZeitgeistEvent* _tmp9_ = NULL;
	ZeitgeistEvent* event;
	GPtrArray* _tmp10_;
	ZeitgeistDataSource* _tmp11_ = NULL;
	ZeitgeistDataSource* ds;
	ZeitgeistDataSource* _tmp12_;
	self = (ZeitgeistDpPlugin*) base;
	_tmp0_ = zeitgeist_log_new ();
	_g_object_unref0 (self->priv->zg_log);
	self->priv->zg_log = _tmp0_;
	_tmp1_ = zeitgeist_data_source_registry_new ();
	_g_object_unref0 (self->priv->zg_registry);
	self->priv->zg_registry = _tmp1_;
	memset (&_tmp2_, 0, sizeof (MediaInfo));
	_tmp3_ = _tmp2_;
	media_info_destroy (&self->priv->current_media);
	self->priv->current_media = _tmp3_;
	_tmp4_ = g_signal_connect_swapped (self->priv->_object, "file-opened", (GCallback) zeitgeist_dp_plugin_file_opened, self);
	_vala_array_add1 (&self->priv->signals, &self->priv->signals_length1, &self->priv->_signals_size_, _tmp4_);
	_tmp5_ = g_signal_connect_swapped (self->priv->_object, "file-closed", (GCallback) zeitgeist_dp_plugin_file_closed, self);
	_vala_array_add2 (&self->priv->signals, &self->priv->signals_length1, &self->priv->_signals_size_, _tmp5_);
	_tmp6_ = g_signal_connect_swapped (self->priv->_object, "metadata-updated", (GCallback) zeitgeist_dp_plugin_metadata_changed, self);
	_vala_array_add3 (&self->priv->signals, &self->priv->signals_length1, &self->priv->_signals_size_, _tmp6_);
	_tmp7_ = g_signal_connect_swapped (self->priv->_object, "notify::playing", (GCallback) zeitgeist_dp_plugin_playing_changed, self);
	_vala_array_add4 (&self->priv->signals, &self->priv->signals_length1, &self->priv->_signals_size_, _tmp7_);
	_tmp8_ = g_ptr_array_new ();
	templates = _tmp8_;
	_tmp9_ = zeitgeist_event_new_full ("", ZEITGEIST_ZG_USER_ACTIVITY, "application://totem.desktop", NULL, NULL);
	event = g_object_ref_sink (_tmp9_);
	g_ptr_array_add (templates, event);
	_tmp10_ = templates;
	templates = NULL;
	_tmp11_ = zeitgeist_data_source_new_full ("org.gnome.Totem,dataprovider", "Totem dataprovider", "Logs access/leave events for media files played with Totem", _tmp10_);
	ds = g_object_ref_sink (_tmp11_);
	_tmp12_ = _g_object_ref0 (ds);
	zeitgeist_data_source_registry_register_data_source (self->priv->zg_registry, _tmp12_, NULL, NULL, NULL);
	_g_object_unref0 (ds);
	_g_object_unref0 (event);
	_g_ptr_array_free0 (templates);
}


static void zeitgeist_dp_plugin_real_deactivate (PeasActivatable* base) {
	ZeitgeistDpPlugin * self;
	self = (ZeitgeistDpPlugin*) base;
	zeitgeist_dp_plugin_file_closed (self, self->priv->_object);
	{
		gulong* id_collection;
		int id_collection_length1;
		int id_it;
		id_collection = self->priv->signals;
		id_collection_length1 = self->priv->signals_length1;
		for (id_it = 0; id_it < self->priv->signals_length1; id_it = id_it + 1) {
			gulong id;
			id = id_collection[id_it];
			{
				g_signal_handler_disconnect (self->priv->_object, id);
			}
		}
	}
	self->priv->signals = (g_free (self->priv->signals), NULL);
	self->priv->signals = NULL;
	self->priv->signals_length1 = 0;
	self->priv->_signals_size_ = 0;
	if (self->priv->media_info_timeout != 0) {
		g_source_remove (self->priv->media_info_timeout);
	}
	if (self->priv->timeout_id != 0) {
		g_source_remove (self->priv->timeout_id);
	}
	self->priv->media_info_timeout = (guint) 0;
	self->priv->timeout_id = (guint) 0;
}


static void zeitgeist_dp_plugin_real_update_state (PeasActivatable* base) {
	ZeitgeistDpPlugin * self;
	self = (ZeitgeistDpPlugin*) base;
}


static gboolean _zeitgeist_dp_plugin_timeout_cb_gsource_func (gpointer self) {
	gboolean result;
	result = zeitgeist_dp_plugin_timeout_cb (self);
	return result;
}


static void zeitgeist_dp_plugin_restart_watcher (ZeitgeistDpPlugin* self, guint interval) {
	guint _tmp0_;
	g_return_if_fail (self != NULL);
	if (self->priv->timeout_id != 0) {
		g_source_remove (self->priv->timeout_id);
	}
	_tmp0_ = g_timeout_add_full (G_PRIORITY_DEFAULT, interval, _zeitgeist_dp_plugin_timeout_cb_gsource_func, g_object_ref (self), g_object_unref);
	self->priv->timeout_id = _tmp0_;
}


static gboolean _zeitgeist_dp_plugin_wait_for_media_info_gsource_func (gpointer self) {
	gboolean result;
	result = zeitgeist_dp_plugin_wait_for_media_info (self);
	return result;
}


static void zeitgeist_dp_plugin_file_opened (ZeitgeistDpPlugin* self, const gchar* mrl, TotemObject* totem) {
	MediaInfo _tmp0_ = {0};
	MediaInfo _tmp1_ = {0};
	gchar* _tmp2_;
	GTimeVal cur_time = {0};
	gint64 _tmp3_;
	g_return_if_fail (self != NULL);
	g_return_if_fail (mrl != NULL);
	g_return_if_fail (totem != NULL);
	if (self->priv->current_media.mrl != NULL) {
		zeitgeist_dp_plugin_file_closed (self, totem);
	}
	memset (&_tmp0_, 0, sizeof (MediaInfo));
	_tmp1_ = _tmp0_;
	media_info_destroy (&self->priv->current_media);
	self->priv->current_media = _tmp1_;
	_tmp2_ = g_strdup (mrl);
	_g_free0 (self->priv->current_media.mrl);
	self->priv->current_media.mrl = _tmp2_;
	g_get_current_time (&cur_time);
	_tmp3_ = zeitgeist_timestamp_from_timeval (&cur_time);
	self->priv->current_media.timestamp = _tmp3_;
	if (self->priv->media_info_timeout == 0) {
		guint _tmp4_;
		_tmp4_ = g_timeout_add_full (G_PRIORITY_DEFAULT, (guint) 250, _zeitgeist_dp_plugin_wait_for_media_info_gsource_func, g_object_ref (self), g_object_unref);
		self->priv->media_info_timeout = _tmp4_;
		zeitgeist_dp_plugin_restart_watcher (self, (guint) 15000);
	}
}


static void zeitgeist_dp_plugin_file_closed (ZeitgeistDpPlugin* self, TotemObject* totem) {
	gboolean _tmp0_ = FALSE;
	g_return_if_fail (self != NULL);
	g_return_if_fail (totem != NULL);
	if (self->priv->current_media.sent_access) {
		_tmp0_ = self->priv->current_media.mrl != NULL;
	} else {
		_tmp0_ = FALSE;
	}
	if (_tmp0_) {
		GTimeVal cur_time = {0};
		gint64 _tmp1_;
		g_get_current_time (&cur_time);
		_tmp1_ = zeitgeist_timestamp_from_timeval (&cur_time);
		self->priv->current_media.timestamp = _tmp1_;
		zeitgeist_dp_plugin_send_event_to_zg (self, TRUE);
		_g_free0 (self->priv->current_media.mrl);
		self->priv->current_media.mrl = NULL;
	}
	if (self->priv->media_info_timeout != 0) {
		g_source_remove (self->priv->media_info_timeout);
	}
	self->priv->media_info_timeout = (guint) 0;
	if (self->priv->timeout_id != 0) {
		g_source_remove (self->priv->timeout_id);
	}
	self->priv->timeout_id = (guint) 0;
}


static void zeitgeist_dp_plugin_metadata_changed (ZeitgeistDpPlugin* self, const gchar* artist, const gchar* title, const gchar* album, guint track_num, TotemObject* totem) {
	g_return_if_fail (self != NULL);
	g_return_if_fail (totem != NULL);
	if (self->priv->media_info_timeout != 0) {
		gchar* _tmp0_;
		gchar* _tmp1_;
		gchar* _tmp2_;
		_tmp0_ = g_strdup (artist);
		_g_free0 (self->priv->current_media.artist);
		self->priv->current_media.artist = _tmp0_;
		_tmp1_ = g_strdup (title);
		_g_free0 (self->priv->current_media.title);
		self->priv->current_media.title = _tmp1_;
		_tmp2_ = g_strdup (album);
		_g_free0 (self->priv->current_media.album);
		self->priv->current_media.album = _tmp2_;
	}
}


static gboolean zeitgeist_dp_plugin_timeout_cb (ZeitgeistDpPlugin* self) {
	gboolean result = FALSE;
	g_return_val_if_fail (self != NULL, FALSE);
	if (self->priv->media_info_timeout != 0) {
		gchar* _tmp0_ = NULL;
		g_source_remove (self->priv->media_info_timeout);
		self->priv->media_info_timeout = (guint) 0;
		_tmp0_ = totem_get_short_title (self->priv->_object);
		_g_free0 (self->priv->current_media.title);
		self->priv->current_media.title = _tmp0_;
		self->priv->timeout_id = (guint) 0;
		zeitgeist_dp_plugin_wait_for_media_info (self);
	}
	self->priv->timeout_id = (guint) 0;
	result = FALSE;
	return result;
}


static void zeitgeist_dp_plugin_query_media_mimetype_data_free (gpointer _data) {
	ZeitgeistDpPluginQueryMediaMimetypeData* data;
	data = _data;
	_g_free0 (data->current_mrl);
	_g_object_unref0 (data->self);
	g_slice_free (ZeitgeistDpPluginQueryMediaMimetypeData, data);
}


static void zeitgeist_dp_plugin_query_media_mimetype (ZeitgeistDpPlugin* self, const gchar* current_mrl, GAsyncReadyCallback _callback_, gpointer _user_data_) {
	ZeitgeistDpPluginQueryMediaMimetypeData* _data_;
	_data_ = g_slice_new0 (ZeitgeistDpPluginQueryMediaMimetypeData);
	_data_->_async_result = g_simple_async_result_new (G_OBJECT (self), _callback_, _user_data_, zeitgeist_dp_plugin_query_media_mimetype);
	g_simple_async_result_set_op_res_gpointer (_data_->_async_result, _data_, zeitgeist_dp_plugin_query_media_mimetype_data_free);
	_data_->self = _g_object_ref0 (self);
	_data_->current_mrl = g_strdup (current_mrl);
	zeitgeist_dp_plugin_query_media_mimetype_co (_data_);
}


static void zeitgeist_dp_plugin_query_media_mimetype_finish (ZeitgeistDpPlugin* self, GAsyncResult* _res_) {
	ZeitgeistDpPluginQueryMediaMimetypeData* _data_;
	_data_ = g_simple_async_result_get_op_res_gpointer (G_SIMPLE_ASYNC_RESULT (_res_));
}


static void zeitgeist_dp_plugin_query_media_mimetype_ready (GObject* source_object, GAsyncResult* _res_, gpointer _user_data_) {
	ZeitgeistDpPluginQueryMediaMimetypeData* data;
	data = _user_data_;
	data->_source_object_ = source_object;
	data->_res_ = _res_;
	zeitgeist_dp_plugin_query_media_mimetype_co (data);
}


static gboolean zeitgeist_dp_plugin_query_media_mimetype_co (ZeitgeistDpPluginQueryMediaMimetypeData* data) {
	switch (data->_state_) {
		case 0:
		goto _state_0;
		case 1:
		goto _state_1;
		default:
		g_assert_not_reached ();
	}
	_state_0:
	data->_tmp0_ = g_strdup (data->current_mrl);
	data->mrl = data->_tmp0_;
	data->_tmp1_ = NULL;
	data->_tmp1_ = g_file_new_for_uri (data->mrl);
	data->f = data->_tmp1_;
	data->_state_ = 1;
	g_file_query_info_async (data->f, G_FILE_ATTRIBUTE_STANDARD_CONTENT_TYPE, 0, G_PRIORITY_DEFAULT_IDLE, NULL, zeitgeist_dp_plugin_query_media_mimetype_ready, data);
	return FALSE;
	_state_1:
	data->_tmp2_ = NULL;
	data->_tmp2_ = g_file_query_info_finish (data->f, data->_res_, &data->_inner_error_);
	data->fi = data->_tmp2_;
	if (data->_inner_error_ != NULL) {
		goto __catch0_g_error;
	}
	if (g_strcmp0 (data->self->priv->current_media.mrl, data->mrl) != 0) {
		data->_tmp3_ = TRUE;
	} else {
		data->_tmp4_ = totem_object_is_playing (data->self->priv->_object);
		data->_tmp3_ = !data->_tmp4_;
	}
	if (data->_tmp3_) {
		_g_object_unref0 (data->fi);
		_g_object_unref0 (data->f);
		_g_free0 (data->mrl);
		if (data->_state_ == 0) {
			g_simple_async_result_complete_in_idle (data->_async_result);
		} else {
			g_simple_async_result_complete (data->_async_result);
		}
		g_object_unref (data->_async_result);
		return FALSE;
	}
	data->_tmp5_ = NULL;
	data->_tmp5_ = g_file_info_get_content_type (data->fi);
	data->_tmp6_ = g_strdup (data->_tmp5_);
	_g_free0 (data->self->priv->current_media.mimetype);
	data->self->priv->current_media.mimetype = data->_tmp6_;
	zeitgeist_dp_plugin_send_event_to_zg (data->self, FALSE);
	data->self->priv->current_media.sent_access = TRUE;
	_g_object_unref0 (data->fi);
	goto __finally0;
	__catch0_g_error:
	{
		data->err = data->_inner_error_;
		data->_inner_error_ = NULL;
		_g_error_free0 (data->err);
	}
	__finally0:
	if (data->_inner_error_ != NULL) {
		_g_object_unref0 (data->f);
		_g_free0 (data->mrl);
		g_critical ("file %s: line %d: uncaught error: %s (%s, %d)", __FILE__, __LINE__, data->_inner_error_->message, g_quark_to_string (data->_inner_error_->domain), data->_inner_error_->code);
		g_clear_error (&data->_inner_error_);
		return FALSE;
	}
	_g_object_unref0 (data->f);
	_g_free0 (data->mrl);
	if (data->_state_ == 0) {
		g_simple_async_result_complete_in_idle (data->_async_result);
	} else {
		g_simple_async_result_complete (data->_async_result);
	}
	g_object_unref (data->_async_result);
	return FALSE;
}


static gboolean zeitgeist_dp_plugin_wait_for_media_info (ZeitgeistDpPlugin* self) {
	gboolean result = FALSE;
	gboolean _tmp0_ = FALSE;
	g_return_val_if_fail (self != NULL, FALSE);
	if (self->priv->current_media.title != NULL) {
		gboolean _tmp1_;
		_tmp1_ = totem_object_is_playing (self->priv->_object);
		_tmp0_ = _tmp1_;
	} else {
		_tmp0_ = FALSE;
	}
	if (_tmp0_) {
		GValue val = {0};
		GtkWidget* _tmp2_ = NULL;
		GtkWidget* _tmp3_;
		BaconVideoWidget* video;
		GValue _tmp4_ = {0};
		const gchar* _tmp5_ = NULL;
		gboolean _tmp6_;
		gchar* _tmp7_;
		_tmp2_ = totem_get_video_widget (self->priv->_object);
		_tmp3_ = _tmp2_;
		video = BACON_IS_VIDEO_WIDGET (_tmp3_) ? ((BaconVideoWidget*) _tmp3_) : NULL;
		bacon_video_widget_get_metadata (video, BVW_INFO_HAS_VIDEO, &_tmp4_);
		G_IS_VALUE (&val) ? (g_value_unset (&val), NULL) : NULL;
		val = _tmp4_;
		_tmp6_ = g_value_get_boolean (&val);
		if (_tmp6_) {
			_tmp5_ = ZEITGEIST_NFO_VIDEO;
		} else {
			_tmp5_ = ZEITGEIST_NFO_AUDIO;
		}
		_tmp7_ = g_strdup (_tmp5_);
		_g_free0 (self->priv->current_media.interpretation);
		self->priv->current_media.interpretation = _tmp7_;
		zeitgeist_dp_plugin_query_media_mimetype (self, self->priv->current_media.mrl, NULL, NULL);
		if (self->priv->timeout_id != 0) {
			g_source_remove (self->priv->timeout_id);
		}
		self->priv->timeout_id = (guint) 0;
		self->priv->media_info_timeout = (guint) 0;
		result = FALSE;
		_g_object_unref0 (video);
		G_IS_VALUE (&val) ? (g_value_unset (&val), NULL) : NULL;
		return result;
	}
	result = TRUE;
	return result;
}


static void zeitgeist_dp_plugin_playing_changed (ZeitgeistDpPlugin* self) {
	gboolean _tmp0_ = FALSE;
	gboolean _tmp1_ = FALSE;
	gboolean _tmp2_;
	g_return_if_fail (self != NULL);
	if (self->priv->media_info_timeout == 0) {
		_tmp0_ = self->priv->current_media.sent_access == FALSE;
	} else {
		_tmp0_ = FALSE;
	}
	if (_tmp0_) {
		zeitgeist_dp_plugin_wait_for_media_info (self);
	}
	_tmp2_ = totem_object_is_playing (self->priv->_object);
	if (!_tmp2_) {
		_tmp1_ = self->priv->current_media.sent_access;
	} else {
		_tmp1_ = FALSE;
	}
	if (_tmp1_) {
		zeitgeist_dp_plugin_file_closed (self, self->priv->_object);
	}
}


static void zeitgeist_dp_plugin_send_event_to_zg (ZeitgeistDpPlugin* self, gboolean leave_event) {
	gboolean _tmp0_ = FALSE;
	g_return_if_fail (self != NULL);
	if (self->priv->current_media.mrl != NULL) {
		_tmp0_ = self->priv->current_media.title != NULL;
	} else {
		_tmp0_ = FALSE;
	}
	if (_tmp0_) {
		const gchar* _tmp1_ = NULL;
		gchar* _tmp2_;
		gchar* event_interpretation;
		gchar* _tmp3_ = NULL;
		gchar* origin;
		const gchar* _tmp4_ = NULL;
		ZeitgeistSubject* _tmp5_ = NULL;
		ZeitgeistSubject* subject;
		ZeitgeistEvent* _tmp6_ = NULL;
		ZeitgeistEvent* event;
		if (leave_event) {
			_tmp1_ = ZEITGEIST_ZG_LEAVE_EVENT;
		} else {
			_tmp1_ = ZEITGEIST_ZG_ACCESS_EVENT;
		}
		_tmp2_ = g_strdup (_tmp1_);
		event_interpretation = _tmp2_;
		_tmp3_ = g_path_get_dirname (self->priv->current_media.mrl);
		origin = _tmp3_;
		_tmp4_ = zeitgeist_manifestation_for_uri (self->priv->current_media.mrl);
		_tmp5_ = zeitgeist_subject_new_full (self->priv->current_media.mrl, self->priv->current_media.interpretation, _tmp4_, self->priv->current_media.mimetype, origin, self->priv->current_media.title, "");
		subject = g_object_ref_sink (_tmp5_);
		_tmp6_ = zeitgeist_event_new_full (event_interpretation, ZEITGEIST_ZG_USER_ACTIVITY, "application://totem.desktop", subject, NULL, NULL);
		event = g_object_ref_sink (_tmp6_);
		zeitgeist_event_set_timestamp (event, self->priv->current_media.timestamp);
		zeitgeist_log_insert_events_no_reply (self->priv->zg_log, event, NULL, NULL);
		_g_object_unref0 (event);
		_g_object_unref0 (subject);
		_g_free0 (origin);
		_g_free0 (event_interpretation);
	}
}


ZeitgeistDpPlugin* zeitgeist_dp_plugin_construct (GType object_type) {
	ZeitgeistDpPlugin * self = NULL;
	self = (ZeitgeistDpPlugin*) g_object_new (object_type, NULL);
	return self;
}


ZeitgeistDpPlugin* zeitgeist_dp_plugin_new (void) {
	return zeitgeist_dp_plugin_construct (TYPE_ZEITGEIST_DP_PLUGIN);
}


TotemObject* zeitgeist_dp_plugin_get_object (ZeitgeistDpPlugin* self) {
	TotemObject* result;
	g_return_val_if_fail (self != NULL, NULL);
	result = self->priv->_object;
	return result;
}


void zeitgeist_dp_plugin_set_object (ZeitgeistDpPlugin* self, TotemObject* value) {
	g_return_if_fail (self != NULL);
	self->priv->_object = value;
	g_object_notify ((GObject *) self, "object");
}


static void zeitgeist_dp_plugin_class_init (ZeitgeistDpPluginClass * klass) {
	zeitgeist_dp_plugin_parent_class = g_type_class_peek_parent (klass);
	g_type_class_add_private (klass, sizeof (ZeitgeistDpPluginPrivate));
	G_OBJECT_CLASS (klass)->get_property = _vala_zeitgeist_dp_plugin_get_property;
	G_OBJECT_CLASS (klass)->set_property = _vala_zeitgeist_dp_plugin_set_property;
	G_OBJECT_CLASS (klass)->finalize = zeitgeist_dp_plugin_finalize;
	g_object_class_install_property (G_OBJECT_CLASS (klass), ZEITGEIST_DP_PLUGIN_OBJECT, g_param_spec_object ("object", "object", "object", TOTEM_TYPE_OBJECT, G_PARAM_STATIC_NAME | G_PARAM_STATIC_NICK | G_PARAM_STATIC_BLURB | G_PARAM_READABLE | G_PARAM_WRITABLE));
}


static void zeitgeist_dp_plugin_peas_activatable_interface_init (PeasActivatableInterface * iface) {
	zeitgeist_dp_plugin_peas_activatable_parent_iface = g_type_interface_peek_parent (iface);
	iface->activate = (void (*)(PeasActivatable*)) zeitgeist_dp_plugin_real_activate;
	iface->deactivate = (void (*)(PeasActivatable*)) zeitgeist_dp_plugin_real_deactivate;
	iface->update_state = (void (*)(PeasActivatable*)) zeitgeist_dp_plugin_real_update_state;
}


static void zeitgeist_dp_plugin_instance_init (ZeitgeistDpPlugin * self) {
	self->priv = ZEITGEIST_DP_PLUGIN_GET_PRIVATE (self);
}


static void zeitgeist_dp_plugin_finalize (GObject* obj) {
	ZeitgeistDpPlugin * self;
	self = ZEITGEIST_DP_PLUGIN (obj);
	media_info_destroy (&self->priv->current_media);
	self->priv->signals = (g_free (self->priv->signals), NULL);
	_g_object_unref0 (self->priv->zg_log);
	_g_object_unref0 (self->priv->zg_registry);
	G_OBJECT_CLASS (zeitgeist_dp_plugin_parent_class)->finalize (obj);
}


GType zeitgeist_dp_plugin_get_type (void) {
	return zeitgeist_dp_plugin_type_id;
}


GType zeitgeist_dp_plugin_register_type (GTypeModule * module) {
	static const GTypeInfo g_define_type_info = { sizeof (ZeitgeistDpPluginClass), (GBaseInitFunc) NULL, (GBaseFinalizeFunc) NULL, (GClassInitFunc) zeitgeist_dp_plugin_class_init, (GClassFinalizeFunc) NULL, NULL, sizeof (ZeitgeistDpPlugin), 0, (GInstanceInitFunc) zeitgeist_dp_plugin_instance_init, NULL };
	static const GInterfaceInfo peas_activatable_info = { (GInterfaceInitFunc) zeitgeist_dp_plugin_peas_activatable_interface_init, (GInterfaceFinalizeFunc) NULL, NULL};
	zeitgeist_dp_plugin_type_id = g_type_module_register_type (module, G_TYPE_OBJECT, "ZeitgeistDpPlugin", &g_define_type_info, 0);
	g_type_module_add_interface (module, zeitgeist_dp_plugin_type_id, PEAS_TYPE_ACTIVATABLE, &peas_activatable_info);
	return zeitgeist_dp_plugin_type_id;
}


static void _vala_zeitgeist_dp_plugin_get_property (GObject * object, guint property_id, GValue * value, GParamSpec * pspec) {
	ZeitgeistDpPlugin * self;
	self = ZEITGEIST_DP_PLUGIN (object);
	switch (property_id) {
		case ZEITGEIST_DP_PLUGIN_OBJECT:
		g_value_set_object (value, zeitgeist_dp_plugin_get_object (self));
		break;
		default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}


static void _vala_zeitgeist_dp_plugin_set_property (GObject * object, guint property_id, const GValue * value, GParamSpec * pspec) {
	ZeitgeistDpPlugin * self;
	self = ZEITGEIST_DP_PLUGIN (object);
	switch (property_id) {
		case ZEITGEIST_DP_PLUGIN_OBJECT:
		zeitgeist_dp_plugin_set_object (self, g_value_get_object (value));
		break;
		default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}


void peas_register_types (GTypeModule* module) {
	GTypeModule* _tmp0_;
	PeasObjectModule* _tmp1_;
	PeasObjectModule* objmodule;
	g_return_if_fail (module != NULL);
	zeitgeist_dp_plugin_register_type (module);
	_tmp0_ = module;
	_tmp1_ = _g_object_ref0 (PEAS_IS_OBJECT_MODULE (_tmp0_) ? ((PeasObjectModule*) _tmp0_) : NULL);
	objmodule = _tmp1_;
	peas_object_module_register_extension_type (objmodule, PEAS_TYPE_ACTIVATABLE, TYPE_ZEITGEIST_DP_PLUGIN);
	_g_object_unref0 (objmodule);
}



